import { Token } from './token';
import { MjPoll } from './mj_poll';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Ability {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: Date })
  notBefore: Date;

  @ApiPropertyOptional({ type: Date })
  notAfter?: Date;

  @ApiProperty({ type: Boolean })
  isRevocable: boolean = true;

  @ApiPropertyOptional({ type: String })
  passwordScheme?: string;

  @ApiPropertyOptional({ type: String })
  password?: string;

  @ApiProperty({ isArray: true, type: () => Token })
  tokens: Token[];

  @ApiProperty({ isArray: true, type: () => MjPoll })
  mjPolls: MjPoll[];
}
