import { User } from './user';
import { Profile } from './profile';
import { MjCandidate } from './mj_candidate';
import { MjParticipate } from './mj_participate';
import { MjBallotBox } from './mj_ballot_box';
import { Ability } from './ability';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class MjPoll {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiPropertyOptional({ type: Date })
  publishedAt?: Date;

  @ApiPropertyOptional({ type: Date })
  voteOpenedAt?: Date;

  @ApiPropertyOptional({ type: Date })
  voteClosedAt?: Date;

  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: String })
  slug: string;

  @ApiProperty({ type: String })
  image: string;

  @ApiProperty({ type: String })
  description: string;

  @ApiProperty({ type: String })
  content: string;

  @ApiProperty({ type: Boolean })
  blankVoteAccepted: boolean = true;

  @ApiPropertyOptional({ type: Number })
  multipleVoteMin?: number;

  @ApiPropertyOptional({ type: Number })
  multipleVoteMax?: number;

  @ApiProperty({ type: Number })
  voteWeightMin: number = -2;

  @ApiProperty({ type: Number })
  voteWeightMax: number = 2;

  @ApiProperty({ type: Number })
  voteCount: number;

  @ApiProperty({ type: Boolean })
  voteCountPublic: boolean;

  @ApiProperty({ type: String })
  votingScheme: string;

  @ApiPropertyOptional({ type: Number })
  ballotBoxVoteCount?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxBlankCount?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightSum?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightAvg?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightMedian?: number;

  @ApiProperty({ type: String })
  createdByUserId: string;

  @ApiPropertyOptional({ type: String })
  profileId?: string;

  @ApiPropertyOptional({ type: String })
  winnerCandidateId?: string;

  @ApiProperty({ type: () => User })
  createdByUser: User;

  @ApiPropertyOptional({ type: () => Profile })
  profile?: Profile;

  @ApiPropertyOptional({ type: () => MjCandidate })
  winnerCandidate?: MjCandidate;

  @ApiProperty({ isArray: true, type: () => MjParticipate })
  participates: MjParticipate[];

  @ApiProperty({ isArray: true, type: () => MjBallotBox })
  ballotBoxes: MjBallotBox[];

  @ApiProperty({ isArray: true, type: () => Ability })
  abilities: Ability[];
}
