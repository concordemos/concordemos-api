import { MjParticipate } from './mj_participate';
import { MjPoll } from './mj_poll';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class MjCandidate {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: String })
  slug: string;

  @ApiProperty({ type: String })
  url: string;

  @ApiPropertyOptional({ type: String })
  image?: string;

  @ApiPropertyOptional({ type: String })
  description?: string;

  @ApiPropertyOptional({ type: String })
  content?: string;

  @ApiProperty({ isArray: true, type: () => MjParticipate })
  participates: MjParticipate[];

  @ApiProperty({ isArray: true, type: () => MjPoll })
  pollWinners: MjPoll[];
}
