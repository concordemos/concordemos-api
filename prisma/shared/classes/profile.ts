import { User } from './user';
import { MjPoll } from './mj_poll';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Profile {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: String })
  name: string;

  @ApiPropertyOptional({ type: String })
  description?: string;

  @ApiProperty({ type: String })
  userId: string;

  @ApiProperty({ type: () => User })
  user: User;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiPropertyOptional({ type: Date })
  deletedAt?: Date;

  @ApiProperty({ isArray: true, type: () => MjPoll })
  polls: MjPoll[];
}
