import { Ability } from './ability';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Token {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: Date })
  notBefore: Date;

  @ApiPropertyOptional({ type: Date })
  notAfter?: Date;

  @ApiProperty({ isArray: true, type: () => Ability })
  abilities: Ability[];
}
