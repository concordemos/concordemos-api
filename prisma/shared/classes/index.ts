import { User as _User } from './user';
import { Profile as _Profile } from './profile';
import { Ability as _Ability } from './ability';
import { Token as _Token } from './token';
import { MjPoll as _MjPoll } from './mj_poll';
import { MjCandidate as _MjCandidate } from './mj_candidate';
import { MjParticipate as _MjParticipate } from './mj_participate';
import { MjBallotBox as _MjBallotBox } from './mj_ballot_box';

export namespace PrismaModel {
  export class User extends _User {}
  export class Profile extends _Profile {}
  export class Ability extends _Ability {}
  export class Token extends _Token {}
  export class MjPoll extends _MjPoll {}
  export class MjCandidate extends _MjCandidate {}
  export class MjParticipate extends _MjParticipate {}
  export class MjBallotBox extends _MjBallotBox {}

  export const extraModels = [
    User,
    Profile,
    Ability,
    Token,
    MjPoll,
    MjCandidate,
    MjParticipate,
    MjBallotBox,
  ];
}
