import { MjPoll } from './mj_poll';
import { MjParticipate } from './mj_participate';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class MjBallotBox {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: String })
  pollId: string;

  @ApiProperty({ type: String })
  participateId: string;

  @ApiPropertyOptional({ type: String })
  userId?: string;

  @ApiProperty({ type: Number })
  weight: number;

  @ApiProperty({ type: () => MjPoll })
  poll: MjPoll;

  @ApiProperty({ type: () => MjParticipate })
  participate: MjParticipate;
}
