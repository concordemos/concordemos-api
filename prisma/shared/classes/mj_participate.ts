import { MjPoll } from './mj_poll';
import { MjCandidate } from './mj_candidate';
import { MjBallotBox } from './mj_ballot_box';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class MjParticipate {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: String })
  pollId: string;

  @ApiProperty({ type: String })
  candidateId: string;

  @ApiPropertyOptional({ type: String })
  image?: string;

  @ApiPropertyOptional({ type: String })
  description?: string;

  @ApiPropertyOptional({ type: String })
  content?: string;

  @ApiPropertyOptional({ type: Number })
  ballotBoxVoteCount?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightSum?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightAvg?: number;

  @ApiPropertyOptional({ type: Number })
  ballotBoxWeightMedian?: number;

  @ApiProperty({ type: () => MjPoll })
  poll: MjPoll;

  @ApiProperty({ type: () => MjCandidate })
  candidate: MjCandidate;

  @ApiProperty({ isArray: true, type: () => MjBallotBox })
  ballotBoxes: MjBallotBox[];
}
