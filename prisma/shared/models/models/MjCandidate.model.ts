import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { MjParticipate, MjPoll } from "./";

export class MjCandidate {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsString()
    name!: string;

    @IsDefined()
    @IsString()
    slug!: string;

    @IsDefined()
    @IsString()
    url!: string;

    @IsOptional()
    @IsString()
    image?: string;

    @IsOptional()
    @IsString()
    description?: string;

    @IsOptional()
    @IsString()
    content?: string;

    @IsDefined()
    participates!: MjParticipate[];

    @IsDefined()
    pollWinners!: MjPoll[];
}
