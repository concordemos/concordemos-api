import { IsString, IsDefined, IsOptional, IsInt } from "class-validator";
import { MjPoll, MjParticipate } from "./";

export class MjBallotBox {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsString()
    pollId!: string;

    @IsDefined()
    @IsString()
    participateId!: string;

    @IsOptional()
    @IsString()
    userId?: string;

    @IsDefined()
    @IsInt()
    weight!: number;

    @IsDefined()
    poll!: MjPoll;

    @IsDefined()
    participate!: MjParticipate;
}
