import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Profile, MjPoll } from "./";

export class User {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsString()
    email!: string;

    @IsDefined()
    @IsString()
    name!: string;

    @IsDefined()
    @IsString()
    password!: string;

    @IsOptional()
    @IsDate()
    activatedAt?: Date;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsOptional()
    @IsDate()
    deletedAt?: Date;

    @IsDefined()
    profiles!: Profile[];

    @IsDefined()
    mjPolls!: MjPoll[];
}
