import { IsString, IsDefined, IsDate, IsOptional, IsBoolean } from "class-validator";
import { Token, MjPoll } from "./";

export class Ability {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsDate()
    notBefore!: Date;

    @IsOptional()
    @IsDate()
    notAfter?: Date;

    @IsDefined()
    @IsBoolean()
    isRevocable!: boolean;

    @IsOptional()
    @IsString()
    passwordScheme?: string;

    @IsOptional()
    @IsString()
    password?: string;

    @IsDefined()
    tokens!: Token[];

    @IsDefined()
    mjPolls!: MjPoll[];
}
