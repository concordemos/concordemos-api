import { IsString, IsDefined, IsDate, IsOptional, IsBoolean, IsInt } from "class-validator";
import { User, Profile, MjCandidate, MjParticipate, MjBallotBox, Ability } from "./";

export class MjPoll {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsOptional()
    @IsDate()
    publishedAt?: Date;

    @IsOptional()
    @IsDate()
    voteOpenedAt?: Date;

    @IsOptional()
    @IsDate()
    voteClosedAt?: Date;

    @IsDefined()
    @IsString()
    name!: string;

    @IsDefined()
    @IsString()
    slug!: string;

    @IsDefined()
    @IsString()
    image!: string;

    @IsDefined()
    @IsString()
    description!: string;

    @IsDefined()
    @IsString()
    content!: string;

    @IsDefined()
    @IsBoolean()
    blankVoteAccepted!: boolean;

    @IsOptional()
    @IsInt()
    multipleVoteMin?: number;

    @IsOptional()
    @IsInt()
    multipleVoteMax?: number;

    @IsDefined()
    @IsInt()
    voteWeightMin!: number;

    @IsDefined()
    @IsInt()
    voteWeightMax!: number;

    @IsDefined()
    @IsInt()
    voteCount!: number;

    @IsDefined()
    @IsBoolean()
    voteCountPublic!: boolean;

    @IsDefined()
    @IsString()
    votingScheme!: string;

    @IsOptional()
    @IsInt()
    ballotBoxVoteCount?: number;

    @IsOptional()
    @IsInt()
    ballotBoxBlankCount?: number;

    @IsOptional()
    @IsInt()
    ballotBoxWeightSum?: number;

    @IsOptional()
    ballotBoxWeightAvg?: number;

    @IsOptional()
    ballotBoxWeightMedian?: number;

    @IsDefined()
    @IsString()
    createdByUserId!: string;

    @IsOptional()
    @IsString()
    profileId?: string;

    @IsOptional()
    @IsString()
    winnerCandidateId?: string;

    @IsDefined()
    createdByUser!: User;

    @IsOptional()
    profile?: Profile;

    @IsOptional()
    winnerCandidate?: MjCandidate;

    @IsDefined()
    participates!: MjParticipate[];

    @IsDefined()
    ballotBoxes!: MjBallotBox[];

    @IsDefined()
    abilities!: Ability[];
}
