import { IsString, IsDefined, IsDate, IsOptional, IsInt } from "class-validator";
import { MjPoll, MjCandidate, MjBallotBox } from "./";

export class MjParticipate {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsString()
    pollId!: string;

    @IsDefined()
    @IsString()
    candidateId!: string;

    @IsOptional()
    @IsString()
    image?: string;

    @IsOptional()
    @IsString()
    description?: string;

    @IsOptional()
    @IsString()
    content?: string;

    @IsOptional()
    @IsInt()
    ballotBoxVoteCount?: number;

    @IsOptional()
    @IsInt()
    ballotBoxWeightSum?: number;

    @IsOptional()
    ballotBoxWeightAvg?: number;

    @IsOptional()
    ballotBoxWeightMedian?: number;

    @IsDefined()
    poll!: MjPoll;

    @IsDefined()
    candidate!: MjCandidate;

    @IsDefined()
    ballotBoxes!: MjBallotBox[];
}
