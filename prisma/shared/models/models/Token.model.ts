import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Ability } from "./";

export class Token {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsDate()
    notBefore!: Date;

    @IsOptional()
    @IsDate()
    notAfter?: Date;

    @IsDefined()
    abilities!: Ability[];
}
