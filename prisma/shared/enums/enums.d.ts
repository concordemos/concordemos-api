export enum ModelNamesUpper {
  User = "User",
  Profile = "Profile",
  Ability = "Ability",
  Token = "Token",
  MjPoll = "MjPoll",
  MjCandidate = "MjCandidate",
  MjParticipate = "MjParticipate",
  MjBallotBox = "MjBallotBox",
}

export enum ModelNamesLower {
  user = "user",
  profile = "profile",
  ability = "ability",
  token = "token",
  mjPoll = "mjPoll",
  mjCandidate = "mjCandidate",
  mjParticipate = "mjParticipate",
  mjBallotBox = "mjBallotBox",
}

export type TModelNames =
  | "User"
  | "Profile"
  | "Ability"
  | "Token"
  | "MjPoll"
  | "MjCandidate"
  | "MjParticipate"
  | "MjBallotBox";
