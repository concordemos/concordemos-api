-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "activatedAt" DATETIME,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "deletedAt" DATETIME
);

-- CreateTable
CREATE TABLE "Profile" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "userId" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "deletedAt" DATETIME,
    CONSTRAINT "Profile_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Ability" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "notBefore" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "notAfter" DATETIME,
    "isRevocable" BOOLEAN NOT NULL DEFAULT true,
    "passwordScheme" TEXT,
    "password" TEXT
);

-- CreateTable
CREATE TABLE "Token" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "notBefore" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "notAfter" DATETIME
);

-- CreateTable
CREATE TABLE "MjPoll" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "publishedAt" DATETIME,
    "voteOpenedAt" DATETIME,
    "voteClosedAt" DATETIME,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "image" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "content" TEXT NOT NULL,
    "blankVoteAccepted" BOOLEAN NOT NULL DEFAULT true,
    "multipleVoteMin" INTEGER,
    "multipleVoteMax" INTEGER,
    "voteWeightMin" INTEGER NOT NULL DEFAULT -2,
    "voteWeightMax" INTEGER NOT NULL DEFAULT 2,
    "voteCount" INTEGER NOT NULL,
    "voteCountPublic" BOOLEAN NOT NULL,
    "votingScheme" TEXT NOT NULL,
    "ballotBoxVoteCount" INTEGER,
    "ballotBoxBlankCount" INTEGER,
    "ballotBoxWeightSum" INTEGER,
    "ballotBoxWeightAvg" REAL,
    "ballotBoxWeightMedian" REAL,
    "createdByUserId" TEXT NOT NULL,
    "profileId" TEXT,
    "winnerCandidateId" TEXT,
    CONSTRAINT "MjPoll_createdByUserId_fkey" FOREIGN KEY ("createdByUserId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "MjPoll_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "MjPoll_winnerCandidateId_fkey" FOREIGN KEY ("winnerCandidateId") REFERENCES "MjCandidate" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "MjCandidate" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "image" TEXT,
    "description" TEXT,
    "content" TEXT
);

-- CreateTable
CREATE TABLE "MjParticipate" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "pollId" TEXT NOT NULL,
    "candidateId" TEXT NOT NULL,
    "image" TEXT,
    "description" TEXT,
    "content" TEXT,
    "ballotBoxVoteCount" INTEGER,
    "ballotBoxWeightSum" INTEGER,
    "ballotBoxWeightAvg" REAL,
    "ballotBoxWeightMedian" REAL,
    CONSTRAINT "MjParticipate_pollId_fkey" FOREIGN KEY ("pollId") REFERENCES "MjPoll" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "MjParticipate_candidateId_fkey" FOREIGN KEY ("candidateId") REFERENCES "MjCandidate" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "MjBallotBox" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "pollId" TEXT NOT NULL,
    "participateId" TEXT NOT NULL,
    "userId" TEXT,
    "weight" INTEGER NOT NULL,
    CONSTRAINT "MjBallotBox_pollId_fkey" FOREIGN KEY ("pollId") REFERENCES "MjPoll" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "MjBallotBox_participateId_fkey" FOREIGN KEY ("participateId") REFERENCES "MjParticipate" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "_AbilityTokenClosure" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    CONSTRAINT "_AbilityTokenClosure_A_fkey" FOREIGN KEY ("A") REFERENCES "Ability" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_AbilityTokenClosure_B_fkey" FOREIGN KEY ("B") REFERENCES "Token" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "_AbilityMjPollClosure" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    CONSTRAINT "_AbilityMjPollClosure_A_fkey" FOREIGN KEY ("A") REFERENCES "Ability" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_AbilityMjPollClosure_B_fkey" FOREIGN KEY ("B") REFERENCES "MjPoll" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "User_name_key" ON "User"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Profile_userId_name_key" ON "Profile"("userId", "name");

-- CreateIndex
CREATE UNIQUE INDEX "MjCandidate_name_key" ON "MjCandidate"("name");

-- CreateIndex
CREATE UNIQUE INDEX "MjCandidate_slug_key" ON "MjCandidate"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "MjCandidate_url_key" ON "MjCandidate"("url");

-- CreateIndex
CREATE UNIQUE INDEX "MjBallotBox_userId_participateId_pollId_key" ON "MjBallotBox"("userId", "participateId", "pollId");

-- CreateIndex
CREATE UNIQUE INDEX "_AbilityTokenClosure_AB_unique" ON "_AbilityTokenClosure"("A", "B");

-- CreateIndex
CREATE INDEX "_AbilityTokenClosure_B_index" ON "_AbilityTokenClosure"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_AbilityMjPollClosure_AB_unique" ON "_AbilityMjPollClosure"("A", "B");

-- CreateIndex
CREATE INDEX "_AbilityMjPollClosure_B_index" ON "_AbilityMjPollClosure"("B");
