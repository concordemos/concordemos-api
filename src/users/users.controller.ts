import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ClassSerializerInterceptor,
  UseGuards,
  UseInterceptors,
  Request,
  ParseUUIDPipe,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Prisma } from '@prisma/client';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UserEntity } from './entities/user.entity';
import { ReqUserDto } from 'src/auth/dto/req-user.dto';

@ApiTags('user')
@Controller('users')
@ApiBearerAuth('token')
@UseGuards(JwtAuthGuard)
export class UsersController {
  constructor(
    // private readonly i18n: I18nService,
    private readonly usersService: UsersService,
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({
    type: ReqUserDto,
    description: `Returns content of the req.user built from JwtToken`,
  })
  @Get('me')
  public async me(@Request() req): Promise<ReqUserDto> {
    return req.user;
  }

  @Get()
  @ApiOkResponse({
    description: 'User',
    type: UserEntity,
    isArray: true,
  })
  findAll() {
    return this.usersService.findAll({});
  }

  @Post()
  @ApiOkResponse({
    description: 'User',
    type: UserEntity,
  })
  @UseInterceptors(ClassSerializerInterceptor)
  async create(@Body() createUserDto: CreateUserDto): Promise<UserEntity> {
    return this.usersService.create(createUserDto);
  }

  @Get(':id')
  @ApiParam({ name: 'id', schema: { type: 'string', format: 'uuid' } })
  @ApiOkResponse({
    description: 'User',
    type: UserEntity,
  })
  @UseInterceptors(ClassSerializerInterceptor)
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.usersService.findOne(<Prisma.UserWhereUniqueInput>{ id });
  }

  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update({
      where: <Prisma.UserWhereUniqueInput>id,
      data: <Prisma.UserUpdateInput>updateUserDto,
    });
  }

  @Delete(':id')
  remove(@Param('id') id: String) {
    return this.usersService.remove(<Prisma.UserWhereUniqueInput>id);
  }
}
