import { PartialType } from '@nestjs/swagger';

import { User as UserModel } from 'prisma/shared/classes/user';

export class UserEntity extends PartialType(UserModel) {
  // @ApiProperty({ required: false })
  // password: string | undefined;

  id: string;

  name: string;

  email: string;
}

// export class User implements UserModel {
//   @ApiProperty({ format: 'uuid' })
//   id: string;

//   @ApiProperty({ example: 'Macron I - Roi des Cons' })
//   name: string;

//   @ApiProperty({ example: 'macron@roi-des.con' })
//   email: string;

//   // @Column({ select: false })
//   // @Exclude()
//   // @IsNotEmpty({ message: 'A password is required' })
//   @ApiProperty()
//   password: string;

//   createdAt: Date;
//   updatedAt: Date;
//   deletedAt: Date;
// }
