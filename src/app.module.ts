import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { loggingMiddleware, PrismaModule } from 'nestjs-prisma';
import { MjModule } from './mj/mj.module';
import { AbilitiesModule } from './abilities/abilities.module';

@Module({
  imports: [
    PrismaModule.forRoot({
      isGlobal: true,
      prismaServiceOptions: {
        middlewares: [
          loggingMiddleware(),
          // async (params, next) => {
          //   // Before query: change params
          //   const result = await next(params);
          //   // After query: result
          //   return result;
          // },
          // usersMiddleware(),
          // async (params, next) => {
          //   if (params.model == 'User') {
          //     if (/^create$/.test(params.action)) {
          //       const user = params.args.data;
          //       const salt = bcrypt.genSaltSync(10);
          //       const hash = bcrypt.hashSync(user.password, salt);
          //       user.password = hash;
          //       params.args.data = user;
          //     }
          //   }
          //   return next(params);
          // },
        ],
      },
    }),
    AuthModule,
    UsersModule,
    MjModule,
    AbilitiesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
