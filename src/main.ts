import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /*
  Issues with enableShutdownHooks#
  Prisma interferes with NestJS enableShutdownHooks. Prisma listens for shutdown signals and will call process.exit() before your application shutdown hooks fire.
  To deal with this, you would need to add a listener for Prisma beforeExit event.
  https://docs.nestjs.com/recipes/prisma
  */

  // brakes watch mode start:dev - it reload with previous saved, not current
  // const prismaService = app.get(PrismaService);
  // await prismaService.enableShutdownHooks(app);

  // enable shutdown hook
  const prismaService: PrismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  // prisma exception filter
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  const config = new DocumentBuilder()
    .setTitle('Concordemos')
    .setDescription('API')
    .setVersion(process.env.npm_package_version || 'nd')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
