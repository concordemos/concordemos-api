import { Prisma } from '@prisma/client';
import * as bcrypt from 'bcrypt';

export const usersMiddlewareDep = (): Prisma.Middleware => {
  return async (params, next) => {
    if (params.model == 'User') {
      if (/^create$/.test(params.action)) {
        const user = params.args.data;

        user.password = await userPasswordHash(user.password);
        params.args.data = user;

        // hide hashed password from response
        const result = await next(params);
        result.password = undefined;

        return result;
      }

      // if (/^find/.test(params.action)) {
      //   let users = await next(params);

      //   users = Array.isArray(users) ? users : [users];

      //   const result = users.map((e) => {
      //     e.password = undefined;
      //     return e;
      //   });

      //   return result;
      // }
    }

    return next(params);
  };
};

export const userPasswordHash = async (pwd: string): Promise<string> => {
  const salt = bcrypt.genSaltSync(10);
  const hash = await bcrypt.hash(pwd, salt);

  return hash;
};
