import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { JwtUserDto } from '../dto/jwt-user.dto';
import { ReqUserDto } from '../dto/req-user.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: process.env.JWT_SECRET || 'secret',
    });
  }

  async validate(payload: JwtUserDto): Promise<ReqUserDto> {
    const reqUser: ReqUserDto = {
      id: payload.sub,
      name: payload.username,
    };

    return reqUser;
  }
}
