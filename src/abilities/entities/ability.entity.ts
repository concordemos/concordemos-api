import { PartialType } from '@nestjs/swagger';
import { Ability } from 'prisma/shared/classes/ability';

export class AbilityEntity extends PartialType(Ability) {}
