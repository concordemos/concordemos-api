import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MjService } from './mj.service';
import { CreateMjDto } from './dto/create-mj.dto';
import { UpdateMjDto } from './dto/update-mj.dto';

@Controller('mj')
export class MjController {
  constructor(private readonly mjService: MjService) {}

  // @Post()
  // create(@Body() createMjDto: CreateMjDto) {
  //   return this.mjService.create(createMjDto);
  // }

  // @Get()
  // findAll() {
  //   return this.mjService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.mjService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateMjDto: UpdateMjDto) {
  //   return this.mjService.update(+id, updateMjDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.mjService.remove(+id);
  // }
}
