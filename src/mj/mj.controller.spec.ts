import { Test, TestingModule } from '@nestjs/testing';
import { MjController } from './mj.controller';
import { MjService } from './mj.service';

describe('MjController', () => {
  let controller: MjController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MjController],
      providers: [MjService],
    }).compile();

    controller = module.get<MjController>(MjController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
