import { Module } from '@nestjs/common';
import { MjService } from './mj.service';
import { MjController } from './mj.controller';
import { MjPublicModule } from './mj-public/mj-public.module';

@Module({
  controllers: [MjController],
  providers: [MjService],
  imports: [MjPublicModule],
})
export class MjModule {}
