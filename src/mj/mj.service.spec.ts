import { Test, TestingModule } from '@nestjs/testing';
import { MjService } from './mj.service';

describe('MjService', () => {
  let service: MjService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MjService],
    }).compile();

    service = module.get<MjService>(MjService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
