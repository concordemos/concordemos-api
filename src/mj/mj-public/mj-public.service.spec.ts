import { Test, TestingModule } from '@nestjs/testing';
import { MjPublicService } from './mj-public.service';

describe('MjPublicService', () => {
  let service: MjPublicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MjPublicService],
    }).compile();

    service = module.get<MjPublicService>(MjPublicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
