import { Module } from '@nestjs/common';
import { MjPublicService } from './mj-public.service';
import { MjPublicController } from './mj-public.controller';

@Module({
  controllers: [MjPublicController],
  providers: [MjPublicService]
})
export class MjPublicModule {}
