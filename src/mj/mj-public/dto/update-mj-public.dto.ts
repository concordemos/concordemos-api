import { PartialType } from '@nestjs/swagger';
import { CreateMjPublicDto } from './create-mj-public.dto';

export class UpdateMjPublicDto extends PartialType(CreateMjPublicDto) {}
