import { Injectable } from '@nestjs/common';
import { CreateMjPublicDto } from './dto/create-mj-public.dto';
import { UpdateMjPublicDto } from './dto/update-mj-public.dto';

@Injectable()
export class MjPublicService {
  create(createMjPublicDto: CreateMjPublicDto) {
    return 'This action adds a new mjPublic';
  }

  findAll() {
    return `This action returns all mjPublic`;
  }

  findOne(id: number) {
    return `This action returns a #${id} mjPublic`;
  }

  update(id: number, updateMjPublicDto: UpdateMjPublicDto) {
    return `This action updates a #${id} mjPublic`;
  }

  remove(id: number) {
    return `This action removes a #${id} mjPublic`;
  }
}
