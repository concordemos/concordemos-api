import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MjPublicService } from './mj-public.service';
import { CreateMjPublicDto } from './dto/create-mj-public.dto';
import { UpdateMjPublicDto } from './dto/update-mj-public.dto';

@Controller('mj/public')
export class MjPublicController {
  constructor(private readonly mjPublicService: MjPublicService) {}

  @Post()
  create(@Body() createMjPublicDto: CreateMjPublicDto) {
    return this.mjPublicService.create(createMjPublicDto);
  }

  @Get()
  findAll() {
    return this.mjPublicService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.mjPublicService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMjPublicDto: UpdateMjPublicDto,
  ) {
    return this.mjPublicService.update(+id, updateMjPublicDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.mjPublicService.remove(+id);
  }
}
