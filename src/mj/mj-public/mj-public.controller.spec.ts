import { Test, TestingModule } from '@nestjs/testing';
import { MjPublicController } from './mj-public.controller';
import { MjPublicService } from './mj-public.service';

describe('MjPublicController', () => {
  let controller: MjPublicController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MjPublicController],
      providers: [MjPublicService],
    }).compile();

    controller = module.get<MjPublicController>(MjPublicController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
