import {
  ClassSerializerInterceptor,
  Injectable,
  NotAcceptableException,
  UseInterceptors,
} from '@nestjs/common';
import { IsUUID } from 'class-validator';
import { PrismaService } from 'nestjs-prisma';
import { UserEntity } from 'src/users/entities/user.entity';
import { CreateMjDto } from './dto/create-mj.dto';
import { UpdateMjDto } from './dto/update-mj.dto';

export class VoteDto {
  @IsUUID()
  poolId: string;

  @IsUUID()
  candidateId: string;

  @IsUUID()
  certId: string;
}

export class VoteResponseDto {}

@Injectable()
export class MjService {
  constructor(private readonly prisma: PrismaService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  async vote(vote: VoteDto): Promise<VoteResponseDto> {
    await this.voteValidate(vote);

    return new VoteResponseDto();
  }

  private async voteValidate(vote: VoteDto): Promise<Boolean> {
    // throw new NotAcceptableException(`Vote did not validate`);
    return true;
  }

  private async voteRegister(vote: VoteDto) {}

  // create(createMjDto: CreateMjDto) {
  //   return 'This action adds a new mj';
  // }

  // findAll() {
  //   return `This action returns all mj`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} mj`;
  // }

  // update(id: number, updateMjDto: UpdateMjDto) {
  //   return `This action updates a #${id} mj`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} mj`;
  // }
}
