import { PartialType } from '@nestjs/swagger';
import { MjCandidate } from 'prisma/shared/classes/mj_candidate';

export class Candidate extends PartialType(MjCandidate) {}
