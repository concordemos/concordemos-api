import { PartialType } from '@nestjs/swagger';
import { MjBallotBox } from 'prisma/shared/classes/mj_ballot_box';

export class BallotBox extends PartialType(MjBallotBox) {}
