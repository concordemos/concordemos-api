import { PartialType } from '@nestjs/swagger';
import { MjPoll } from 'prisma/shared/classes/mj_poll';

export class Poll extends PartialType(MjPoll) {}
