import { PartialType } from '@nestjs/swagger';
import { MjParticipate } from 'prisma/shared/classes/mj_participate';

export class Participate extends PartialType(MjParticipate) {}
