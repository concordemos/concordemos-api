import { PartialType } from '@nestjs/swagger';
import { CreateMjDto } from './create-mj.dto';

export class UpdateMjDto extends PartialType(CreateMjDto) {}
